﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using poisk.webapp.Database;
using poisk.webapp.Middlewares;
using System;

namespace poisk.webapp.tests.IntegrationTests
{
	public class BaseDbTest
	{
		protected MainDbContext Context;
		protected IMapper Mapper;

		[OneTimeSetUp]
		public void Init()
		{
			var options = new DbContextOptionsBuilder<MainDbContext>()
				.UseInMemoryDatabase(Guid.NewGuid().ToString())
				.Options;

			var configurationProvider = new MapperConfiguration(cfg =>
			{
				cfg.AddProfile<AutoMapperProfile>();
			});

			Context = new MainDbContext(options);
			Mapper = configurationProvider.CreateMapper();
		}

		[OneTimeTearDown]
		public void OneTimeTearDown()
		{
			Context?.Dispose();
		}
	}
}
