﻿using Microsoft.Extensions.Logging;
using Moq;
using NUnit.Framework;
using poisk.webapp.Models;
using poisk.webapp.Services;
using Shouldly;

namespace poisk.webapp.tests.IntegrationTests.Services
{
	public class ProductServiceTests : BaseDbTest
	{
		private const int productId = 1;
		private IProductService productService;

		[SetUp]
		public void SetUp()
		{
			productService = new ProductService(Context, Mapper, new Mock<ILogger<ProductService>>().Object);
		}

		[Test]
		[Order(1)]
		public void Add_ProductValid_ProductSaved()
		{
			var product = new ProductViewModel
			{
				Id = productId,
				Article = "test",
				Name = "name",
				Price = 2,
				Unit = "unit",
				UnitsPerPlace = 1,
				WeightPerUnit = 3,
				IsLicenseRequired = true
			};

			var response = productService.Add(product);

			response.ShouldNotBeNull();
			response.IsSuccess.ShouldBeTrue();
			response.Data.ShouldBeOfType(typeof(ProductViewModel));

			var savedProduct = Context.Products.Find(response.Data.Id);

			savedProduct.Article.ShouldBe(product.Article);
			savedProduct.IsLicenseRequired.ShouldBe(product.IsLicenseRequired);
			savedProduct.Name.ShouldBe(product.Name);
			savedProduct.Price.ShouldBe(product.Price);
			savedProduct.Unit.ShouldBe(product.Unit);
			savedProduct.UnitsPerPlace.ShouldBe(product.UnitsPerPlace);
			savedProduct.WeightPerUnit.ShouldBe(product.WeightPerUnit);
		}

		[Test]
		[Order(2)]
		public void Get_ProductsExist_ProductsReturned()
		{
			var response = productService.Get();
			response.ShouldNotBeNull();
			response.IsSuccess.ShouldBeTrue();
			response.Data.ShouldHaveSingleItem();
		}

		[Test]
		[Order(3)]
		public void Update_ProductValid_ProductUpdated()
		{
			var product = new ProductViewModel
			{
				Id = productId,
				Article = "test updated",
				Name = "name updated",
				Price = 4,
				Unit = "unit upd",
				UnitsPerPlace = 2,
				WeightPerUnit = 4,
				IsLicenseRequired = true
			};

			var response = productService.Update(product);

			response.ShouldNotBeNull();
			response.IsSuccess.ShouldBeTrue();

			var savedProduct = Context.Products.Find(productId);

			savedProduct.Article.ShouldBe(product.Article);
			savedProduct.IsLicenseRequired.ShouldBe(product.IsLicenseRequired);
			savedProduct.Name.ShouldBe(product.Name);
			savedProduct.Price.ShouldBe(product.Price);
			savedProduct.Unit.ShouldBe(product.Unit);
			savedProduct.UnitsPerPlace.ShouldBe(product.UnitsPerPlace);
			savedProduct.WeightPerUnit.ShouldBe(product.WeightPerUnit);
		}

		[Test]
		[Order(4)]
		public void Delete_ProductsExist_ProductsDeleted()
		{
			var response = productService.Delete(productId);
			response.ShouldNotBeNull();
			response.IsSuccess.ShouldBeTrue();

			var savedProduct = Context.Products.Find(productId);
			savedProduct.ShouldBeNull();
		}
	}
}
