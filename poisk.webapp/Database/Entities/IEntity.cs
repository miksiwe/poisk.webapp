﻿namespace poisk.webapp.Database.Entities
{
    public interface IEntity
    {
        int Id { get; set; }
    }
}
