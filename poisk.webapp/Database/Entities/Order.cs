﻿using System;
using System.Collections.Generic;

namespace poisk.webapp.Database.Entities
{
    public class Order : IEntity
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public DateTime CreatedAt { get; set; }
		public DateTime? FinishedAt { get; set; }
		public string State { get; set; }
        public string Phone { get; set; }
        public string DeliveryAddress { get; set; }
        public string Note { get; set; }
		public decimal Discount { get; set; }
		public string AgreementNumber { get; set; }
		public string CertificateNumber { get; set; }
		public bool IsLicenseProductsGrouped { get; set; }

		public virtual List<OrderItem> OrderItems { get; set; }
        public virtual Customer Customer { get; set; }
    }
}
