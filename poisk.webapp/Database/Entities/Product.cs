﻿namespace poisk.webapp.Database.Entities
{
    public class Product : IEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Unit { get; set; }
        public decimal Price { get; set; }
        public string Article { get; set; }
        public decimal WeightPerUnit { get; set; }
        public decimal UnitsPerPlace { get; set; }
        public bool IsLicenseRequired { get; set; }
    }
}
