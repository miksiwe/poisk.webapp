﻿using poisk.webapp.Middlewares;
using System;

namespace poisk.webapp.Database.Entities
{
    public class Customer : IEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string DeliveryAddress { get; set; }
        public string ContractNumber { get; set; }
        public string UNP { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public CustomerType Type { get; set; }
        public string FullName { get; set; }
        public string Delegate { get; set; }
        public string PassportNumber { get; set; }
        public DateTime? BirthDate { get; set; }
        public string PassportUniqueCode { get; set; }
        public string RegistrationAddress { get; set; }
        public string Note { get; set; }
        public bool IsPrepayment { get; set; }
        public string CheckingAccount { get; set; }
        public string BankName { get; set; }
        public string BankAddress { get; set; }
        public string BankCode { get; set; }
        public DateTime ContractDate { get; set; }
    }
}
