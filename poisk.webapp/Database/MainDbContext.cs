﻿using Microsoft.EntityFrameworkCore;
using poisk.webapp.Database.Entities;
using System.Linq;

namespace poisk.webapp.Database
{
    public class MainDbContext : DbContext
    {
        public MainDbContext(DbContextOptions<MainDbContext> options)
            : base(options)
        {
            if (Database.ProviderName != "Microsoft.EntityFrameworkCore.InMemory")
            {
                Database.Migrate();
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Product>().HasKey(x => x.Id);
            modelBuilder.Entity<Product>().Property(x => x.Name).HasMaxLength(256).IsRequired();
            modelBuilder.Entity<Product>().Property(x => x.Price).HasColumnType("decimal(10,4)").IsRequired();
            modelBuilder.Entity<Product>().Property(x => x.Unit).HasMaxLength(64).IsRequired();
            modelBuilder.Entity<Product>().Property(x => x.Article).HasMaxLength(64).IsRequired();
            modelBuilder.Entity<Product>().Property(x => x.UnitsPerPlace).HasColumnType("decimal(10,4)").IsRequired();
            modelBuilder.Entity<Product>().Property(x => x.WeightPerUnit).HasColumnType("decimal(10,4)").IsRequired();
            modelBuilder.Entity<Product>().Property(x => x.IsLicenseRequired).IsRequired();

            modelBuilder.Entity<Customer>().HasKey(x => x.Id);
            modelBuilder.Entity<Customer>().Property(x => x.Name).HasMaxLength(256).IsRequired();
            modelBuilder.Entity<Customer>().Property(x => x.Type).IsRequired();
            modelBuilder.Entity<Customer>().Property(x => x.Note).HasMaxLength(1024);
            modelBuilder.Entity<Customer>().Property(x => x.IsPrepayment).IsRequired();
            modelBuilder.Entity<Customer>().Property(x => x.BirthDate);
            modelBuilder.Entity<Customer>().Property(x => x.PassportNumber).HasMaxLength(18);
            modelBuilder.Entity<Customer>().Property(x => x.PassportUniqueCode).HasMaxLength(20);
            modelBuilder.Entity<Customer>().Property(x => x.Phone).HasMaxLength(13).IsRequired();
            modelBuilder.Entity<Customer>().Property(x => x.RegistrationAddress).HasMaxLength(512);
            modelBuilder.Entity<Customer>().Property(x => x.UNP).HasMaxLength(18);
            modelBuilder.Entity<Customer>().Property(x => x.Address).HasMaxLength(512).IsRequired();
            modelBuilder.Entity<Customer>().Property(x => x.ContractNumber).HasMaxLength(32);
            modelBuilder.Entity<Customer>().Property(x => x.Delegate).HasMaxLength(64);
            modelBuilder.Entity<Customer>().Property(x => x.DeliveryAddress).HasMaxLength(512).IsRequired();
            modelBuilder.Entity<Customer>().Property(x => x.Email).HasMaxLength(128);
            modelBuilder.Entity<Customer>().Property(x => x.FullName).HasMaxLength(128);
            modelBuilder.Entity<Customer>().Property(x => x.BankAddress).HasMaxLength(512);
            modelBuilder.Entity<Customer>().Property(x => x.BankCode).HasMaxLength(20);
            modelBuilder.Entity<Customer>().Property(x => x.BankName).HasMaxLength(256);
            modelBuilder.Entity<Customer>().Property(x => x.CheckingAccount).HasMaxLength(42);
            modelBuilder.Entity<Customer>().Property(x => x.ContractDate).IsRequired();

            modelBuilder.Entity<OrderItem>().HasKey(x => x.Id);
            modelBuilder.Entity<OrderItem>().Property(x => x.Quantity).IsRequired();
            modelBuilder.Entity<OrderItem>().Property(x => x.OrderId).IsRequired();
            modelBuilder.Entity<OrderItem>().Property(x => x.ProductId).IsRequired();
            modelBuilder.Entity<OrderItem>().HasOne(x => x.Product);

            modelBuilder.Entity<Order>().HasKey(x => x.Id);
            modelBuilder.Entity<Order>().Property(x => x.CreatedAt).IsRequired();
            modelBuilder.Entity<Order>().Property(x => x.CustomerId).IsRequired();
            modelBuilder.Entity<Order>().Property(x => x.Discount).HasColumnType("decimal(10,4)").IsRequired();
            modelBuilder.Entity<Order>().Property(x => x.FinishedAt);
            modelBuilder.Entity<Order>().Property(x => x.DeliveryAddress).IsRequired();
            modelBuilder.Entity<Order>().Property(x => x.Note);
            modelBuilder.Entity<Order>().Property(x => x.AgreementNumber);
            modelBuilder.Entity<Order>().Property(x => x.CertificateNumber);
            modelBuilder.Entity<Order>().Property(x => x.State).IsRequired();
            modelBuilder.Entity<Order>().Property(x => x.Phone).IsRequired();
            modelBuilder.Entity<Order>().Property(x => x.IsLicenseProductsGrouped).IsRequired();
            modelBuilder.Entity<Order>().HasOne(x => x.Customer);
            modelBuilder.Entity<Order>().HasMany(x => x.OrderItems)
	            .WithOne(x => x.Order).OnDelete(DeleteBehavior.Cascade);

            base.OnModelCreating(modelBuilder);
        }

        public DbSet<Product> Products { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderItem> OrderItems { get; set; }
        public DbSet<Customer> Customers { get; set; }
    }

    public static class ContextExtension
    {
        public static void DetachLocal<T>(this DbContext context, T t)
            where T : class, IEntity
        {
            var local = context.Set<T>()
                .Local
                .FirstOrDefault(entry => entry.Id.Equals(t.Id));
            if (local != null)
            {
                context.Entry(local).State = EntityState.Detached;
            }
        }
    }
}
