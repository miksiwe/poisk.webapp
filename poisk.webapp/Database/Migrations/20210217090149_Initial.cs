﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace poisk.webapp.Database.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Customers",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(maxLength: 256, nullable: false),
                    Address = table.Column<string>(maxLength: 512, nullable: false),
                    DeliveryAddress = table.Column<string>(maxLength: 512, nullable: false),
                    ContractNumber = table.Column<string>(maxLength: 32, nullable: true),
                    UNP = table.Column<string>(maxLength: 18, nullable: true),
                    Phone = table.Column<string>(maxLength: 13, nullable: false),
                    Email = table.Column<string>(maxLength: 128, nullable: true),
                    Type = table.Column<int>(nullable: false),
                    FullName = table.Column<string>(maxLength: 128, nullable: true),
                    Delegate = table.Column<string>(maxLength: 64, nullable: true),
                    PassportNumber = table.Column<string>(maxLength: 18, nullable: true),
                    BirthDate = table.Column<DateTime>(nullable: true),
                    PassportUniqueCode = table.Column<string>(maxLength: 20, nullable: true),
                    RegistrationAddress = table.Column<string>(maxLength: 512, nullable: true),
                    Note = table.Column<string>(maxLength: 1024, nullable: true),
                    IsPrepayment = table.Column<bool>(nullable: false),
                    CheckingAccount = table.Column<string>(maxLength: 42, nullable: true),
                    BankName = table.Column<string>(maxLength: 256, nullable: true),
                    BankAddress = table.Column<string>(maxLength: 512, nullable: true),
                    BankCode = table.Column<string>(maxLength: 20, nullable: true),
                    ContractDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Customers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Products",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(maxLength: 256, nullable: false),
                    Unit = table.Column<string>(maxLength: 64, nullable: false),
                    Price = table.Column<decimal>(type: "decimal(10,2)", nullable: false),
                    Article = table.Column<string>(maxLength: 64, nullable: false),
                    WeightPerUnit = table.Column<decimal>(type: "decimal(10,4)", nullable: false),
                    UnitsPerPlace = table.Column<decimal>(type: "decimal(10,4)", nullable: false),
                    IsLicenseRequired = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Products", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Orders",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CustomerId = table.Column<int>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    FinishedAt = table.Column<DateTime>(nullable: true),
                    State = table.Column<string>(nullable: false),
                    Phone = table.Column<string>(nullable: false),
                    DeliveryAddress = table.Column<string>(nullable: false),
                    Note = table.Column<string>(nullable: true),
                    Discount = table.Column<decimal>(type: "decimal(10,4)", nullable: false),
                    AgreementNumber = table.Column<string>(nullable: true),
                    CertificateNumber = table.Column<string>(nullable: true),
                    IsLicenseProductsGrouped = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Orders", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Orders_Customers_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "Customers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "OrderItems",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ProductId = table.Column<int>(nullable: false),
                    Quantity = table.Column<int>(nullable: false),
                    OrderId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrderItems", x => x.Id);
                    table.ForeignKey(
                        name: "FK_OrderItems_Orders_OrderId",
                        column: x => x.OrderId,
                        principalTable: "Orders",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_OrderItems_Products_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Products",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_OrderItems_OrderId",
                table: "OrderItems",
                column: "OrderId");

            migrationBuilder.CreateIndex(
                name: "IX_OrderItems_ProductId",
                table: "OrderItems",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_CustomerId",
                table: "Orders",
                column: "CustomerId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "OrderItems");

            migrationBuilder.DropTable(
                name: "Orders");

            migrationBuilder.DropTable(
                name: "Products");

            migrationBuilder.DropTable(
                name: "Customers");
        }
    }
}
