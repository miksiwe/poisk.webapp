﻿namespace poisk.webapp.Models
{
	public class InvoiceViewModel
	{
		public string Car { get; set; }
		public string Driver { get; set; }
		public string Waybill { get; set; }
		public string TTNNumber { get; set; }
	}
}
