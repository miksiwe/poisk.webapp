﻿namespace poisk.webapp.Models
{
    public class ServiceResponseModel
    {
        public bool IsSuccess { get; set; }

        public string ErrorMessage { get; set; }
    }

    public class ServiceResponseModel<T> : ServiceResponseModel
    {
        public T Data { get; set; }
    }
}
