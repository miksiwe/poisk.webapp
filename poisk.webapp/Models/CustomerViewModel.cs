﻿using poisk.webapp.Middlewares;
using System;
using System.ComponentModel.DataAnnotations;

namespace poisk.webapp.Models
{
    public class CustomerViewModel
    {
        [Key]
        public int Id { get; set; }

        [Display(Name = "Наименование")]
        [Required(ErrorMessage = "Обязательное поле")]
        public string Name { get; set; }

        [Display(Name = "Юр. адрес")]
        [Required(ErrorMessage = "Обязательное поле")]
        public string Address { get; set; }

        [Display(Name = "Адрес доставки")]
        [Required(ErrorMessage = "Обязательное поле")]
        public string DeliveryAddress { get; set; }

        [Display(Name = "Договор")]
        public string ContractNumber { get; set; } = $"{DateTime.Now.Day}/{DateTime.Now.Month}";

        [Display(Name = "УНП")]
        [MaxLength(9, ErrorMessage = "Не более 9 символов")]
        public string UNP { get; set; }

        [Display(Name = "Телефон")]
        [Phone]
        [MaxLength(13, ErrorMessage = "Не более 13 символов")]
        [Required(ErrorMessage = "Обязательное поле")]
        public string Phone { get; set; }

        [Display(Name = "Почта")]
        [EmailAddress]
        public string Email { get; set; }

        [Display(Name = "Тип клиента")]
        [Required(ErrorMessage = "Обязательное поле")]
        public CustomerType Type { get; set; } = CustomerType.Organisation;

        [Display(Name = "Полное имя")]
        public string FullName { get; set; }

        [Display(Name = "Кем выдан")]
        public string Delegate { get; set; }

        [Display(Name = "Номер паспорта")]
        [MaxLength(9, ErrorMessage = "Не более 9 символов")]
        public string PassportNumber { get; set; }

        [Display(Name = "Дата рождения")]
        public DateTime? BirthDate { get; set; }

        [Display(Name = "Код паспорта")]
        [MaxLength(14, ErrorMessage = "Не более 14 символов")]
        public string PassportUniqueCode { get; set; }

        [Display(Name = "Прописка")]
        public string RegistrationAddress { get; set; }

        [Display(Name = "Комментарий")]
        public string Note { get; set; }

        [Display(Name = "Работа по предоплате")]
        public bool IsPrepayment { get; set; }

        [Display(Name = "Расчетный счет")]
        public string CheckingAccount { get; set; }

        [Display(Name = "Банк")]
        public string BankName { get; set; }

        [Display(Name = "Адрес банка")]
        public string BankAddress { get; set; }

        [Display(Name = "БИК")]
        public string BankCode { get; set; }

        [Display(Name = "Тип клиента")]
        public string CustomerTypeName => Type.GetDescription();

        [Display(Name = "Дата заключения договора")]
        [Required(ErrorMessage = "Обязательное поле")]
        public DateTime ContractDate { get; set; } = DateTime.Now;

	}
}
