﻿using System.ComponentModel.DataAnnotations;

namespace poisk.webapp.Models
{
	public class OrderItemViewModel
	{
		[Required(ErrorMessage = "Обязательное поле")]
		public int ProductId { get; set; }
		[Required(ErrorMessage = "Обязательное поле")]
		public int Quantity { get; set; }
		[Required(ErrorMessage = "Обязательное поле")]
		public int OrderId { get; set; }

		public ProductViewModel Product { get; set; }

		public string ProductName => Product?.Name;
	}
}
