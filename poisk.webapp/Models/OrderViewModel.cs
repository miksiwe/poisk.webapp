using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using poisk.webapp.Middlewares;

namespace poisk.webapp.Models
{
	public class OrderViewModel
	{
		[Key]
		public int Id { get; set; }

		[Required(ErrorMessage = "Обязательное поле")]
		public int CustomerId { get; set; }

		[Required(ErrorMessage = "Обязательное поле")]
		[Display(Name = "Дата создания")]
		public DateTime CreatedAt { get; set; }

		[Display(Name = "Дата отгрузки")]
		public DateTime? FinishedAt { get; set; }

		[Required(ErrorMessage = "Обязательное поле")]
		[Display(Name = "Статус")]
		public string State { get; set; } = SharedData.NewOrderStatus;

		[Required(ErrorMessage = "Обязательное поле")]
		[Display(Name = "Телефон")]
		public string Phone { get; set; }

		[Required(ErrorMessage = "Обязательное поле")]
		[Display(Name = "Адрес доставки")]
		public string DeliveryAddress { get; set; }

		[Display(Name = "Комментарий")]
		public string Note { get; set; }

		[Required(ErrorMessage = "Обязательное поле")]
		[Range(0.0, 1)]
		[Display(Name = "Скидка")]
		public decimal Discount { get; set; } = 0m;

		public decimal Nds { get; set; } = 0.2m;

		[Required(ErrorMessage = "Обязательное поле")]
		[Display(Name = "Документы")] // для заголовка в гриде
		public string AgreementNumber { get; set; }

		public string CertificateNumber { get; set; }

		[Display(Name = "Группировать дробь и картечь в документах")]
		[Required(ErrorMessage = "Обязательное поле")]
		public bool IsLicenseProductsGrouped { get; set; } = true;

		public List<OrderItemViewModel> OrderItems { get; set; } = new List<OrderItemViewModel>();
		public CustomerViewModel Customer { get; set; }

		public string Title => $"Заказ {Id} - {CustomerName}";
		public string Summary
		{
			get
			{
				var summary = new StringBuilder();
				foreach (var orderItem in OrderItems)
				{
					summary.AppendLine($"{orderItem.Product.Name} - {orderItem.Quantity} {orderItem.Product.Unit}");
				}

				summary.AppendLine($"Всего мест: {TotalPlaces}");
				summary.AppendLine($"Вес: {TotalWeight}");

				return summary.ToString();
			}
		}

		[Display(Name = "Общая информация")]
		public string ItemsSummary => $"Всего позиций: {OrderItems.Count} на сумму {TotalPriceWithNds} руб. \nВес: {TotalWeight}, Мест: {TotalPlaces}";

		public decimal ItemsSum => Math.Round(OrderItems.Sum(x => (x.Product.Price - x.Product.Price * Discount) * x.Quantity), 2);

		public decimal NdsPrice => Math.Round(ItemsSum * Nds, 2);

		public decimal TotalPriceWithNds => ItemsSum + NdsPrice;

		[Display(Name = "Клиент")]
		public string CustomerName => Customer?.Name;
		public bool IsCertificateNeeded => OrderItems.Any(x => (x.Product?.IsLicenseRequired).GetValueOrDefault());
		public decimal TotalWeight => OrderItems.Sum(x => Math.Round(x.Product.WeightPerUnit * x.Quantity, 0));
		public decimal TotalPlaces
		{
			get
			{
				var totalPlacesLicenseProducts = 0m;

				if (IsCertificateNeeded)
				{
					var totalQuantityLicenseProducts = OrderItems.Where(x => x.Product.IsLicenseRequired).Sum(x => x.Quantity);
					totalPlacesLicenseProducts = Math.Round(totalQuantityLicenseProducts / OrderItems
						.First(x => x.Product.IsLicenseRequired).Product.UnitsPerPlace, 0, MidpointRounding.ToPositiveInfinity);
				}
				
				return totalPlacesLicenseProducts +
					OrderItems
					.Where(x => !x.Product.IsLicenseRequired)
					.Sum(x => Math.Round(x.Quantity / x.Product.UnitsPerPlace, 0));
			}
		}
	}
}
