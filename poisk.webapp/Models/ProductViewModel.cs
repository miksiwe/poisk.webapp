﻿using System.ComponentModel.DataAnnotations;

namespace poisk.webapp.Models
{
    public class ProductViewModel
    {
        [Key]
        public int Id { get; set; }

        [Display(Name = "Наименование")]
        [Required(ErrorMessage = "Обязательное поле")]
        public string Name { get; set; }

        [Display(Name = "Ед. измерения")]
        [Required(ErrorMessage = "Обязательное поле")]
        public string Unit { get; set; }

        [Display(Name = "Цена")]
        [Required(ErrorMessage = "Обязательное поле")]
        public decimal Price { get; set; }

        [Display(Name = "Артикул")]
        [Required(ErrorMessage = "Обязательное поле")]
        public string Article { get; set; }

        [Display(Name = "Вес за единицу (для ТТН)")]
        [Required(ErrorMessage = "Обязательное поле")]
        public decimal WeightPerUnit { get; set; }

        [Display(Name = "Товара на одно место (для ТТН)")]
        [Required(ErrorMessage = "Обязательное поле")]
        public decimal UnitsPerPlace { get; set; }

        [Display(Name = "Требуется удостоверение")]
        [Required(ErrorMessage = "Обязательное поле")]
        public bool IsLicenseRequired { get; set; }  

    }
}
