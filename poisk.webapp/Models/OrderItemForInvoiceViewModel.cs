﻿using System;

namespace poisk.webapp.Models
{
	public class OrderItemForInvoiceViewModel
	{
		public string Name { get; set; }
		public string Article { get; set; }
		public string Unit { get; set; }
		public int Quantity { get; set; }
		public decimal Price { get; set; }
		public decimal TotalPrice => Math.Round(Price * Quantity, 2);
		public decimal Nds { get; set; } = 0.2m;
		public string NdsString => (Nds * 100).ToString("n0");
		public decimal NdsPrice => Math.Round(TotalPrice * Nds, 2);
		public decimal TotalPriceWithNds => TotalPrice + NdsPrice;
		public decimal TotalPlaces { get; set; }
		public decimal TotalWeight { get; set; }
		public string Note { get; set; } = string.Empty;
	}
}
