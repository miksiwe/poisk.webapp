﻿using Syncfusion.Blazor;
using System.Resources;

namespace poisk.webapp.Middlewares
{
    public class BaseLocaliser : ISyncfusionStringLocalizer
    {
        public string GetText(string key)
        {
            return this.ResourceManager.GetString(key);
        }

        public ResourceManager ResourceManager
        {
            get
            {
                return Resources.SfResources.ResourceManager;
            }
        }
    }
}
