﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace poisk.webapp.Middlewares
{
	public class SharedData
	{
		public static IEnumerable<string> OrderStatuses => 
			new [] { NewOrderStatus, InProgressOrderStatus, DoneOrderStatus };

		public const string NewOrderStatus = "Новый";
		public const string InProgressOrderStatus = "Отгрузка";
		public const string DoneOrderStatus = "Выполнен";
	}

	public enum CustomerType
	{
		[Description("Юр. лицо")]
		Organisation = 1,

		[Description("ИП")]
		SelfEmployed = 2,

		[Description("Физ. лицо")]
		Person = 3
	}

	public class EnumEnvelope
	{
		public CustomerType Value { get; set; }
		public string Name { get; set; }
	}

	public static class EnumExtensions
	{
		public static string GetDescription(this Enum value)
		{
			var field = value.GetType().GetField(value.ToString());
			var attributes = field.GetCustomAttributes(typeof(DescriptionAttribute), false) as DescriptionAttribute[];

			if (attributes != null && attributes.Any())
			{
				return attributes.First().Description;
			}

			return value.ToString();
		}
	}
}
