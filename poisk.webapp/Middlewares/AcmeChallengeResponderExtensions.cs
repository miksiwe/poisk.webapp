﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.FileProviders;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace poisk.webapp.Middlewares
{
    public static class AcmeChallengeResponderExtensions
    {
        private const string WellKnownFolder = ".well-known";
        private const string WellKnownRequestPath = "/.well-known";

        public static IApplicationBuilder UseAcmeChallengeResponder(this IApplicationBuilder app, string rootFolderPath)
        {
            if (rootFolderPath == null)
            {
                throw new ArgumentNullException(nameof(rootFolderPath));
            }

            var root = new DirectoryInfo(rootFolderPath);
            if (!root.Exists)
            {
                throw new ArgumentException("The provided folder does not exist");
            }

            var wellKnownFolder = root.CreateSubdirectory(WellKnownFolder);

            app.UseStaticFiles(new StaticFileOptions
            {
                RequestPath = WellKnownRequestPath,
                FileProvider = new PhysicalFileProvider(wellKnownFolder.FullName),
                ServeUnknownFileTypes = true,
                DefaultContentType = "text/plain"
            });

            return app;
        }
    }
}
