﻿using Microsoft.JSInterop;
using System.Threading.Tasks;

namespace poisk.webapp.Middlewares
{
    public class Spinner
    {
        private readonly IJSRuntime jsRuntime;

        public Spinner(IJSRuntime jsRuntime)
        {
            this.jsRuntime = jsRuntime;
        }

        public async Task ShowAsync()
        {
            await jsRuntime.InvokeAsync<object>("startLoading");
        }

        public async Task HideAsync()
        {
            await jsRuntime.InvokeAsync<object>("stopLoading");
        }
    }
}
