﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using poisk.webapp.Database;
using poisk.webapp.Database.Entities;
using poisk.webapp.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace poisk.webapp.Services
{
    public interface IProductService
    {
        ServiceResponseModel<IEnumerable<ProductViewModel>> Get();

        ServiceResponseModel<ProductViewModel> Add(ProductViewModel product);

        ServiceResponseModel Update(ProductViewModel product);

        ServiceResponseModel Delete(int id);
    }

    public class ProductService : IProductService
    {
        private readonly MainDbContext context;
        private readonly ILogger logger;
        private readonly IMapper mapper;

        public ProductService(MainDbContext context, IMapper mapper, ILogger<ProductService> logger)
        {
            this.context = context ?? throw new ArgumentNullException(nameof(context));
            this.logger = logger ?? throw new ArgumentNullException(nameof(logger));
            this.mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public ServiceResponseModel<ProductViewModel> Add(ProductViewModel product)
        {
            var response = new ServiceResponseModel<ProductViewModel>();
            if (product == null)
            {
                return response;
            }

            var entity = mapper.Map<Product>(product);

            try
            {
                context.Add(entity);
                context.SaveChanges();

                product.Id = entity.Id;
                response.IsSuccess = true;
                response.Data = product;
                return response;
            }
            catch (Exception e)
            {
                logger.LogError(e, "Unable to save product");
                response.ErrorMessage = "Не удалось добавить продукт";
                return response;
            }
        }

        public ServiceResponseModel Delete(int id)
        {
            var response = new ServiceResponseModel();
            if (id == default)
            {
                return response;
            }

            try
            {
                var entity = context.Products.Single(x => x.Id == id);
                context.DetachLocal(entity);
                context.Remove(entity);
                context.SaveChanges();

                response.IsSuccess = true;
                return response;
            }
            catch (Exception e)
            {
                logger.LogError(e, "Unable to remove product");
                response.ErrorMessage = "Не удалось удалить продукт";
                return response;
            }

        }

        public ServiceResponseModel<IEnumerable<ProductViewModel>> Get()
        {
            var response = new ServiceResponseModel<IEnumerable<ProductViewModel>>();

            try
            {
                var entities = context.Products.AsNoTracking().ToList();
                var products = entities.Select(x => mapper.Map<ProductViewModel>(x));

                response.IsSuccess = true;
                response.Data = products;
                return response;
            }
            catch (Exception e)
            {
                logger.LogError(e, "Unable to get products");
                response.ErrorMessage = "Не удалось загрузить продукты";
                return response;
            }
        }

        public ServiceResponseModel Update(ProductViewModel product)
        {
            var response = new ServiceResponseModel();
            if (product == null)
            {
                return response;
            }

            var entity = mapper.Map<Product>(product);

            try
            {
                context.DetachLocal(entity);
                context.Products.Update(entity);
                context.SaveChanges();

                response.IsSuccess = true;
                return response;
            }
            catch (Exception e)
            {
                logger.LogError(e, "Unable to save product");
                response.ErrorMessage = "Не удалось добавить продукт";
                return response;
            }
        }
    }
}
