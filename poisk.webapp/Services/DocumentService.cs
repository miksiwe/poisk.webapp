﻿using Microsoft.Extensions.Logging;
using poisk.webapp.Models;
using Syncfusion.DocIO;
using Syncfusion.DocIO.DLS;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using poisk.webapp.Middlewares;
using Syncfusion.XlsIO;
using Humanizer;
using System.Text.RegularExpressions;
using System.IO.Compression;

namespace poisk.webapp.Services
{
    public interface IDocumentService
    {
        ServiceResponseModel<Stream> GetContract(CustomerViewModel customer);

		ServiceResponseModel<Stream> GetCertificate(OrderViewModel order);

		ServiceResponseModel<Stream> GetAgreement(OrderViewModel order);

		ServiceResponseModel<Stream> GetInvoice(OrderViewModel order, InvoiceViewModel invoice);

		// returns Base64 string потому что по другому нихуя не работает :(
		ServiceResponseModel<string> GetDocumentsPack(IEnumerable<OrderViewModel> order, InvoiceViewModel invoice);
	}

	public class DocumentService : IDocumentService
	{
		private const string ProductsWithSertificateCommonName = "Дробь охотничья и картечь";
		private const string ProductWithSertificateCommonArticle = "ГОСТ 7837-76";
		private const string DrobName = "Дробь";
		private const string KartechName = "Картечь";
		private const int CertificateDefaultTableLenght = 11;
		private readonly IEnumerable<string> DrobValues = new[] { "7", "6", "5", "4", "3", "2", "1", "0", "00", "000", "0000" };
		private readonly IEnumerable<string> KartechValues = new[] { "5,60", "5,90", "6,20", "6,50", "7,15", "7,55", "8,00", "8,50", "8,80" };
		private readonly ILogger logger;

		public DocumentService(ILogger<DocumentService> logger)
		{
			this.logger = logger ?? throw new ArgumentNullException(nameof(logger));
		}

		public ServiceResponseModel<Stream> GetAgreement(OrderViewModel order)
		{
			var result = new ServiceResponseModel<Stream>();

			try
			{
				using (WordDocument document = new WordDocument())
				{
					//Opens the input Word document.
					var filePath = Path.GetFullPath(@"DocTemplates/AgreementTemplate.doc");
					Stream docStream = File.OpenRead(filePath);
					document.Open(docStream, FormatType.Doc);
					docStream.Dispose();

					// Replace

					document.Replace(DocumentConsts.AgreementNumberTemplate, order.AgreementNumber, true, true);
					
					var date = DateTime.Now;
					var dateInfo = CultureInfo.GetCultureInfo("ru-RU").DateTimeFormat;
					var dateString = $"«{date.Day}» {dateInfo.MonthGenitiveNames[date.Month - 1]} {date.Year} г.";
					document.Replace(DocumentConsts.DateTemplate, dateString, true, true);

					document.Replace(DocumentConsts.ContractNumberTemplate, order.Customer.ContractNumber, true, true);

					var contractDate = order.Customer.ContractDate;
					var contractDateString = $"«{contractDate.Day}» {dateInfo.MonthGenitiveNames[contractDate.Month - 1]} {contractDate.Year} г.";
					document.Replace(DocumentConsts.ContractDateTemplare, contractDateString, true, true);

					#region multiple table

					var multipleTable = new WTable(document);

					var row = multipleTable.AddRow();
					row.IsHeader = true;
					var cell = row.AddCell();
					cell.Width = 100;
					var p = cell.AddParagraph();
					p.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Center;
					var text = p.AppendText("Наименование и краткая характеристика товара");
					text.CharacterFormat.FontSize = 9;

					cell = row.AddCell();
					cell.Width = 70;
					p = cell.AddParagraph();
					p.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Center;
					text = p.AppendText("Артикул, марка, тип, сорт");
					text.CharacterFormat.FontSize = 9;

					cell = row.AddCell();
					cell.Width = 40;
					p = cell.AddParagraph();
					p.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Center;
					text = p.AppendText("Единица измерения");
					text.CharacterFormat.FontSize = 9;

					cell = row.AddCell();
					cell.Width = 40;
					p = cell.AddParagraph();
					p.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Center;
					text = p.AppendText("Объем партии");
					text.CharacterFormat.FontSize = 9;

					cell = row.AddCell();
					cell.Width = 70;
					p = cell.AddParagraph();
					p.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Center;
					text = p.AppendText("Свободная отпускная цена предприятия-изготовителя, руб.");
					text.CharacterFormat.FontSize = 9;

					cell = row.AddCell();
					cell.Width = 45;
					p = cell.AddParagraph();
					p.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Center;
					text = p.AppendText("Ставка НДС, %");
					text.CharacterFormat.FontSize = 9;

					cell = row.AddCell();
					cell.Width = 45;
					p = cell.AddParagraph();
					p.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Center;
					text = p.AppendText("Сумма НДС, руб.");
					text.CharacterFormat.FontSize = 9;

					cell = row.AddCell();
					cell.Width = 80;
					p = cell.AddParagraph();
					p.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Center;
					text = p.AppendText("Отпускная цена предприятия-изготовителя с учетом НДС, руб.");
					text.CharacterFormat.FontSize = 9;

					var items = GetInvoicePositions(order);
					var counter = 1;

					foreach (var item in items)
					{
						var index = 0;
						row = multipleTable.AddRow();
						cell = row.Cells[index++];
						p = cell.AddParagraph();
						p.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Left;
						text = p.AppendText($"{counter}. {item.Name}");
						text.CharacterFormat.FontSize = 9;

						cell = row.Cells[index++];
						p = cell.AddParagraph();
						p.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Center;
						text = p.AppendText(item.Article);
						text.CharacterFormat.FontSize = 9;

						cell = row.Cells[index++];
						p = cell.AddParagraph();
						p.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Center;
						text = p.AppendText(item.Unit);
						text.CharacterFormat.FontSize = 9;

						cell = row.Cells[index++];
						p = cell.AddParagraph();
						p.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Center;
						text = p.AppendText(item.Quantity.ToString());
						text.CharacterFormat.FontSize = 9;

						cell = row.Cells[index++];
						p = cell.AddParagraph();
						p.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Center;
						text = p.AppendText(item.TotalPrice.ToString("n2"));
						text.CharacterFormat.FontSize = 9;

						cell = row.Cells[index++];
						p = cell.AddParagraph();
						p.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Center;
						text = p.AppendText(item.NdsString);
						text.CharacterFormat.FontSize = 9;

						cell = row.Cells[index++];
						p = cell.AddParagraph();
						p.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Center;
						text = p.AppendText(item.NdsPrice.ToString("n2"));
						text.CharacterFormat.FontSize = 9;

						cell = row.Cells[index++];
						p = cell.AddParagraph();
						p.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Center;
						text = p.AppendText(item.TotalPriceWithNds.ToString("n2"));
						text.CharacterFormat.FontSize = 9;

						counter++;
					}

					row = multipleTable.AddRow();
					multipleTable.ApplyHorizontalMerge(row.GetRowIndex(), 0, 3);

					cell = row.Cells[0];
					p = cell.AddParagraph();
					p.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Center;
					text = p.AppendText("ИТОГО:");
					text.CharacterFormat.FontSize = 11;
					text.CharacterFormat.Bold = true;

					cell = row.Cells[4];
					p = cell.AddParagraph();
					p.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Center;
					text = p.AppendText(items.Sum(x => x.TotalPrice).ToString("n2"));
					text.CharacterFormat.FontSize = 11;
					text.CharacterFormat.Bold = true;

					cell = row.Cells[6];
					p = cell.AddParagraph();
					p.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Center;
					text = p.AppendText(items.Sum(x => x.NdsPrice).ToString("n2"));
					text.CharacterFormat.FontSize = 11;
					text.CharacterFormat.Bold = true;

					cell = row.Cells[7];
					p = cell.AddParagraph();
					p.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Center;
					text = p.AppendText(items.Sum(x => x.TotalPriceWithNds).ToString("n2"));
					text.CharacterFormat.FontSize = 11;
					text.CharacterFormat.Bold = true;

					#endregion

					#region single table

					var singleTable = new WTable(document);

					row = singleTable.AddRow();
					row.IsHeader = true;
					cell = row.AddCell();
					cell.Width = 100;
					p = cell.AddParagraph();
					p.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Center;
					text = p.AppendText("Наименование и краткая характеристика товара");
					text.CharacterFormat.FontSize = 9;

					cell = row.AddCell();
					cell.Width = 70;
					p = cell.AddParagraph();
					p.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Center;
					text = p.AppendText("Артикул, марка, тип, сорт");
					text.CharacterFormat.FontSize = 9;

					cell = row.AddCell();
					cell.Width = 40;
					p = cell.AddParagraph();
					p.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Center;
					text = p.AppendText("Единица измерения");
					text.CharacterFormat.FontSize = 9;

					cell = row.AddCell();
					cell.Width = 40;
					p = cell.AddParagraph();
					p.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Center;
					text = p.AppendText("Объем партии");
					text.CharacterFormat.FontSize = 9;

					cell = row.AddCell();
					cell.Width = 70;
					p = cell.AddParagraph();
					p.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Center;
					text = p.AppendText("Свободная отпускная цена предприятия-изготовителя, руб.");
					text.CharacterFormat.FontSize = 9;

					cell = row.AddCell();
					cell.Width = 45;
					p = cell.AddParagraph();
					p.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Center;
					text = p.AppendText("Ставка НДС, %");
					text.CharacterFormat.FontSize = 9;

					cell = row.AddCell();
					cell.Width = 45;
					p = cell.AddParagraph();
					p.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Center;
					text = p.AppendText("Сумма НДС, руб.");
					text.CharacterFormat.FontSize = 9;

					cell = row.AddCell();
					cell.Width = 80;
					p = cell.AddParagraph();
					p.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Center;
					text = p.AppendText("Отпускная цена предприятия-изготовителя с учетом НДС, руб.");
					text.CharacterFormat.FontSize = 9;

					var singleItems = GetInvoiceSinglePositions(order);
					counter = 1;

					foreach (var item in singleItems)
					{
						var index = 0;
						row = singleTable.AddRow();
						cell = row.Cells[index++];
						p = cell.AddParagraph();
						p.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Left;
						text = p.AppendText($"{counter}. {item.Name}");
						text.CharacterFormat.FontSize = 9;

						cell = row.Cells[index++];
						p = cell.AddParagraph();
						p.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Center;
						text = p.AppendText(item.Article);
						text.CharacterFormat.FontSize = 9;

						cell = row.Cells[index++];
						p = cell.AddParagraph();
						p.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Center;
						text = p.AppendText(item.Unit);
						text.CharacterFormat.FontSize = 9;

						cell = row.Cells[index++];
						p = cell.AddParagraph();
						p.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Center;
						text = p.AppendText(item.Quantity.ToString());
						text.CharacterFormat.FontSize = 9;

						cell = row.Cells[index++];
						p = cell.AddParagraph();
						p.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Center;
						text = p.AppendText(item.Price.ToString("n2"));
						text.CharacterFormat.FontSize = 9;

						cell = row.Cells[index++];
						p = cell.AddParagraph();
						p.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Center;
						text = p.AppendText(item.NdsString);
						text.CharacterFormat.FontSize = 9;

						cell = row.Cells[index++];
						p = cell.AddParagraph();
						p.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Center;
						text = p.AppendText(item.NdsPrice.ToString("n2"));
						text.CharacterFormat.FontSize = 9;

						cell = row.Cells[index++];
						p = cell.AddParagraph();
						p.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Center;
						text = p.AppendText(item.TotalPriceWithNds.ToString("n2"));
						text.CharacterFormat.FontSize = 9;

						counter++;
					}

					#endregion

					//Replaces the table placeholder text with a new table
					var bodyPart = new TextBodyPart(document);
					bodyPart.BodyItems.Add(multipleTable);
					document.Replace("{multipleTable}", bodyPart, true, true, false);
					bodyPart = new TextBodyPart(document);
					bodyPart.BodyItems.Add(singleTable);
					document.Replace("{singleTable}", bodyPart, true, true, false);

					//Saves the file
					docStream = new MemoryStream();
					document.Save(docStream, FormatType.Doc);

					result.Data = docStream;
					result.IsSuccess = true;
				}
			}
			catch (Exception ex)
			{
				result.ErrorMessage = "Звони Максу, шото поломалось :)";
				logger.LogError(ex, $"Failed to create agreement for order {order.Id}");
			}

			return result;
		}

		public ServiceResponseModel<Stream> GetCertificate(OrderViewModel order)
		{
			var result = new ServiceResponseModel<Stream>();

			try
			{
				using (WordDocument document = new WordDocument())
				{
					//Opens the input Word document.
					var filePath = Path.GetFullPath(@"DocTemplates/CertificateTemplate.doc");
					Stream docStream = File.OpenRead(filePath);
					document.Open(docStream, FormatType.Docx);
					docStream.Dispose();

					// Replace
					document.Replace(DocumentConsts.CertificateNumberTemplate, order.CertificateNumber, true, true);

					var date = DateTime.Now;
					var dateInfo = CultureInfo.GetCultureInfo("ru-RU").DateTimeFormat;
					var dateString = $"«{date.Day}» {dateInfo.MonthGenitiveNames[date.Month - 1]} {date.Year} г.";
					document.Replace(DocumentConsts.DateTemplate, dateString, true, true);

					document.Replace(DocumentConsts.CustomerNameTemplate, order.Customer.Name, true, true);

					document.Replace(DocumentConsts.ContractNumberTemplate, order.Customer.ContractNumber, true, true);

					var contractDateString = $"{order.Customer.ContractDate:dd.MM.yyyy} г.";
					document.Replace(DocumentConsts.ContractDateTemplare, contractDateString, true, true);

					document.Replace(DocumentConsts.AgreementNumberTemplate, order.AgreementNumber, true, true);

					var productExample = order.OrderItems.First(x => x.Product.IsLicenseRequired).Product;
					var totalQuantity = order.OrderItems.Where(x => x.Product.IsLicenseRequired).Sum(x => x.Quantity);
					document.Replace(DocumentConsts.TotalWeightTemplate, $"{totalQuantity:n0}", true, true);

					var regex = new Regex(@"\d+");

					#region drob table

					var drobTable = new WTable(document);

					var row = drobTable.AddRow();
					row.IsHeader = true;
					var cell = row.AddCell();
					cell.CellFormat.VerticalAlignment = VerticalAlignment.Middle;
					var p = cell.AddParagraph();
					p.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Center;
					var text = p.AppendText(DrobName);
					text.CharacterFormat.FontSize = 11;

					row = drobTable.AddRow();
					cell = row.Cells[0];
					cell.CellFormat.VerticalAlignment = VerticalAlignment.Middle;
					p = cell.AddParagraph();
					p.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Center;
					text = p.AppendText("№");
					text.CharacterFormat.FontSize = 11;

					var drobItems = order.OrderItems
						.Where(x => x.Product.IsLicenseRequired && x.Product.Name.Contains(DrobName));
					var drobAvailableValues = GetCertificateTableHeaders(drobItems, DrobValues);

					foreach (var item in drobAvailableValues)
					{
						cell = row.AddCell();
						cell.CellFormat.VerticalAlignment = VerticalAlignment.Middle;
						p = cell.AddParagraph();
						p.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Center;
						text = p.AppendText(item);
						text.CharacterFormat.FontSize = 11;
					}

					row = drobTable.AddRow();
					cell = row.Cells[0];
					cell.CellFormat.VerticalAlignment = VerticalAlignment.Middle;
					p = cell.AddParagraph();
					p.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Center;
					text = p.AppendText("Вес, кг.");
					text.CharacterFormat.FontSize = 11;

					var counter = 1;
					foreach (var item in drobAvailableValues)
					{
						var product = drobItems.FirstOrDefault(x => regex.Match(x.Product.Name).Value == item);

						cell = row.Cells[counter];
						cell.CellFormat.VerticalAlignment = VerticalAlignment.Middle;
						p = cell.AddParagraph();
						p.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Center;
						text = p.AppendText(product != null ? $"{product.Quantity}" : "-");
						text.CharacterFormat.FontSize = 11;

						counter++;
					}

					#endregion

					#region kartech table

					var kartechTable = new WTable(document);

					row = kartechTable.AddRow();
					row.IsHeader = true;
					cell = row.AddCell();
					cell.CellFormat.VerticalAlignment = VerticalAlignment.Middle;
					p = cell.AddParagraph();
					p.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Center;
					text = p.AppendText(KartechName);
					text.CharacterFormat.FontSize = 11;

					row = kartechTable.AddRow();
					cell = row.Cells[0];
					cell.CellFormat.VerticalAlignment = VerticalAlignment.Middle;
					p = cell.AddParagraph();
					p.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Center;
					text = p.AppendText("№");
					text.CharacterFormat.FontSize = 11;

					var kartechItems = order.OrderItems
						.Where(x => x.Product.IsLicenseRequired && x.Product.Name.Contains(KartechName));
					var kartechAvailableValues = GetCertificateTableHeaders(kartechItems, KartechValues);

					foreach (var item in kartechAvailableValues)
					{
						cell = row.AddCell();
						cell.CellFormat.VerticalAlignment = VerticalAlignment.Middle;
						p = cell.AddParagraph();
						p.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Center;
						text = p.AppendText(item);
						text.CharacterFormat.FontSize = 11;
					}

					row = kartechTable.AddRow();
					cell = row.Cells[0];
					cell.CellFormat.VerticalAlignment = VerticalAlignment.Middle;
					p = cell.AddParagraph();
					p.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Center;
					text = p.AppendText("Вес, кг.");
					text.CharacterFormat.FontSize = 11;

					counter = 1;
					foreach (var item in kartechAvailableValues)
					{
						var product = kartechItems.FirstOrDefault(x => x.Product.Name.Contains(item));

						cell = row.Cells[counter];
						cell.CellFormat.VerticalAlignment = VerticalAlignment.Middle;
						p = cell.AddParagraph();
						p.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Center;
						text = p.AppendText(product != null ? $"{product.Quantity}" : "-");
						text.CharacterFormat.FontSize = 11;

						counter++;
					}

					#endregion

					//Replaces the table placeholder text with a new table
					var bodyPart = new TextBodyPart(document);
					bodyPart.BodyItems.Add(drobTable);
					document.Replace("{drobTable}", bodyPart, true, true, false);

					bodyPart = new TextBodyPart(document);
					bodyPart.BodyItems.Add(kartechTable);
					document.Replace("{kartechTable}", bodyPart, true, true, false);

					//Saves the file
					docStream = new MemoryStream();
					document.Save(docStream, FormatType.Doc);

					result.Data = docStream;
					result.IsSuccess = true;
				}
			}
			catch (Exception ex)
			{
				result.ErrorMessage = "Звони Максу, шото поломалось :)";
				logger.LogError(ex, $"Failed to create certificate for order {order.Id}");
			}

			return result;
		}

		public ServiceResponseModel<Stream> GetContract(CustomerViewModel customer)
		{
			var result = new ServiceResponseModel<Stream>();

			try
			{
				using (WordDocument document = new WordDocument())
				{
					//Opens the input Word document.
					var filePath = Path.GetFullPath(@"DocTemplates/ContractTemplate.doc");
					Stream docStream = File.OpenRead(filePath);
					document.Open(docStream, FormatType.Doc);
					docStream.Dispose();

					// Replace
					// Contract Number
					document.Replace(DocumentConsts.ContractNumberTemplate, customer.ContractNumber, true, true);
					// Date
					var date = customer.ContractDate;
					var dateInfo = CultureInfo.GetCultureInfo("ru-RU").DateTimeFormat;
					var dateString = $"«{date.Day}» {dateInfo.MonthGenitiveNames[date.Month-1]} {date.Year}";
					document.Replace(DocumentConsts.DateTemplate, dateString, true, true);
					// Customer Name
					var customerName = customer.Type == CustomerType.Person
						? GetCustomerData(customer, true)
						: customer.Name;
					document.Replace(DocumentConsts.CustomerNameTemplate, customerName, true, true);
					// Delegate
					var @delegate = customer.Type == CustomerType.Organisation
						? $"в лице {customer.Delegate},"
						: string.Empty;
					document.Replace(DocumentConsts.DelegateTemplate, @delegate, true, true);
					// Act reason
					var actReason = customer.Type == CustomerType.Organisation
						? $"{DocumentConsts.OrganisationActReason},"
						: customer.Type == CustomerType.SelfEmployed
							? DocumentConsts.SelfEmployedActReason + customer.UNP + ","
							: string.Empty;
					document.Replace(DocumentConsts.ActReasonTemplate, actReason, true, true);
					// Buy reason
					var buyReason = customer.Type == CustomerType.Person
						? DocumentConsts.PersonBuyReason
						: DocumentConsts.OrganisationBuyReason;
					document.Replace(DocumentConsts.BuyReasonTemplate, buyReason, true, true);
					// Payment type
					var paymentType = customer.IsPrepayment
						? DocumentConsts.Prepayment
						: DocumentConsts.DefaultPayment;
					document.Replace(DocumentConsts.PaymentTypeTemplate, paymentType, true, true);
					// Contract End Date
					document.Replace(DocumentConsts.ContractEndDateTemplare, $"{date.Day} {dateInfo.MonthGenitiveNames[date.Month - 1]} {date.Year + 1}", true, true);
					// Customer Data
					document.Replace(DocumentConsts.CustomerDataTemplate, GetCustomerData(customer), true, true);
					//Saves the file
					docStream = new MemoryStream();
					document.Save(docStream, FormatType.Doc);

					result.Data = docStream;
					result.IsSuccess = true;
				}
			}
			catch (Exception ex)
			{
				result.ErrorMessage = "Звони Максу, шото поломалось :)";
				logger.LogError(ex, $"Failed to create contract for customer {customer.Id}, {customer.Name}");
			}

			return result;
		}

		public ServiceResponseModel<Stream> GetInvoice(OrderViewModel order, InvoiceViewModel invoice)
		{
			var result = new ServiceResponseModel<Stream>();

			try
			{
				using (var excelEngine = new ExcelEngine())
				{
					var filePath = Path.GetFullPath(@"DocTemplates/InvoiceTemplate.xlsx");
					// Loads or open an existing workbook through Open method of IWorkbooks
					var inputStream = new FileStream(filePath, FileMode.Open);
					var workbook = excelEngine.Excel.Workbooks.Open(inputStream);
					inputStream.Dispose();

					// insert data
					var positions = GetInvoicePositions(order);

					var firstSheet = workbook.Worksheets["1"];
					firstSheet.Range[DocumentConsts.CustomerUnpCell].Text = order.Customer.UNP;

					var currentDate = DateTime.Now;
					var dateInfo = CultureInfo.GetCultureInfo("ru-RU").DateTimeFormat;
					firstSheet.Range[DocumentConsts.InvoiceDayCell].Text = currentDate.Day.ToString("00");
					firstSheet.Range[DocumentConsts.InvoiceMonthCell].Text = dateInfo.MonthGenitiveNames[currentDate.Month - 1];
					firstSheet.Range[DocumentConsts.InvoiceYearCell].Text = (currentDate.Year-2000).ToString();

					firstSheet.Range[DocumentConsts.CarCell].Text = invoice.Car;
					firstSheet.Range[DocumentConsts.WaybillCell].Text = invoice.Waybill;
					firstSheet.Range[DocumentConsts.DriverFirstCell].Text = invoice.Driver;
					firstSheet.Range[DocumentConsts.CustomerNameAndAddressCell].Text = $"{order.Customer.Name} {order.Customer.Address}";
					firstSheet.Range[DocumentConsts.ContractNumberAndDateCell].Text = $"Договор №{order.Customer.ContractNumber} от {order.Customer.ContractDate:dd.MM.yyyy} г.";
					firstSheet.Range[DocumentConsts.DeliveryAddressCell].Text = order.DeliveryAddress;
					firstSheet.Range[DocumentConsts.DeliveryAddressCell].CellStyle.Font.Size =
						order.DeliveryAddress.Length >= 40
							? 6
							: order.DeliveryAddress.Length >= 30
								? 8
								: 10;

					var totalNdsAmount = positions.Sum(x => x.NdsPrice);
					var totalAmountNdsIncluded = positions.Sum(x => x.TotalPriceWithNds);
					var totalWeight = positions.Sum(x => x.TotalWeight);
					var totalPlaces = positions.Sum(x => x.TotalPlaces);
					var nfi = new CultureInfo("ru-RU", false).NumberFormat;

					firstSheet.Range[DocumentConsts.TotalAmountTableCell].Text = positions.Sum(x => x.Quantity).ToString("n0");
					firstSheet.Range[DocumentConsts.TotalPriceTableCell].Text = positions.Sum(x => x.TotalPrice).ToString(nfi);
					firstSheet.Range[DocumentConsts.TotalNdsAmountTableCell].Text = totalNdsAmount.ToString(nfi);
					firstSheet.Range[DocumentConsts.TotalAmountWithNdsTableCell].Text = totalAmountNdsIncluded.ToString(nfi);
					firstSheet.Range[DocumentConsts.TotalPlacesTableCell].Text = totalPlaces.ToString("n0");
					firstSheet.Range[DocumentConsts.TotalWeightTableCell].Text = totalWeight.ToString("n0");

					firstSheet.Range[DocumentConsts.TotalNdsAmountRubCell].Text = FirstToUpper(((int)totalNdsAmount).ToWords());
					firstSheet.Range[DocumentConsts.TotalNdsAmountCentsCell].Text = ((totalNdsAmount - (int)totalNdsAmount) * 100).ToString("n0");
					firstSheet.Range[DocumentConsts.TotalAmountNdsIncludingRubCell].Text = FirstToUpper(((int)totalAmountNdsIncluded).ToWords());
					firstSheet.Range[DocumentConsts.TotalAmountNdsIncludingCentsCell].Text = ((totalAmountNdsIncluded - (int)totalAmountNdsIncluded) * 100).ToString("n0");

					//var secondSheet = workbook.Worksheets["2"];
					firstSheet.Range[DocumentConsts.TotalWeightCell].Text = $"{FirstToUpper(((int)totalWeight).ToWords())} кг.";
					firstSheet.Range[DocumentConsts.TotalPlacesCell].Text = FirstToUpper(((int)totalPlaces).ToWords());

					firstSheet.Range[DocumentConsts.DriverSecondCell].Text = $"Водитель {invoice.Driver}";
					var ttnNumberStr = string.IsNullOrWhiteSpace(invoice.TTNNumber) ? "" : $"ТТН {invoice.TTNNumber} от {currentDate:dd.MM.yyyy} г.";
					firstSheet.Range[DocumentConsts.DocsAttachedCell].Text = $"{ttnNumberStr}";

					// insert positions
					var marker = workbook.CreateTemplateMarkersProcessor();
					marker.AddVariable("Items", positions);
					marker.ApplyMarkers();

					//Saving the workbook
					workbook.Version = ExcelVersion.Xlsx;
					var outputStream = new MemoryStream();
					workbook.SaveAs(outputStream);

					result.Data = outputStream;
					result.IsSuccess = true;
				}
			}
			catch (Exception ex)
			{
				result.ErrorMessage = "Звони Максу, шото поломалось :)";
				logger.LogError(ex, $"Failed to create invoice for order {order.Id}");
			}

			return result;
		}

		public ServiceResponseModel<string> GetDocumentsPack(IEnumerable<OrderViewModel> orders, InvoiceViewModel invoice)
		{
			var result = new ServiceResponseModel<string>();

			if (!orders.Any())
			{
				result.ErrorMessage = "Нет заявок для формирования документов";
				return result;
			}

			try
			{
				using (var memoryStream = new MemoryStream())
				{
					using (var archive = new ZipArchive(memoryStream, ZipArchiveMode.Create, true))
					{
						var streams = new Dictionary<string, Stream>();
						foreach (var order in orders)
						{
							if (order.IsCertificateNeeded)
							{
								streams.Add($"{order.Customer.Name}/Удостоверение-{order.AgreementNumber}.doc", GetCertificate(order).Data);
							}

							streams.Add($"{order.Customer.Name}/Соглашение-{order.AgreementNumber}.doc", GetAgreement(order).Data);
							streams.Add($"{order.Customer.Name}/ТТН-{order.AgreementNumber}.xls", GetInvoice(order, invoice).Data);
						}

						foreach (var stream in streams)
						{
							var reg = new Regex("[*'\",_&#^@]");
							var name = reg.Replace(stream.Key, string.Empty);
							var doc = archive.CreateEntry(name, CompressionLevel.Optimal);
							using (var entryStream = doc.Open())
							using (var writer = new StreamWriter(entryStream))
							{
								stream.Value.Seek(0, SeekOrigin.Begin);
								stream.Value.CopyTo(entryStream);
							}
						}
					}

					result.Data = Convert.ToBase64String(memoryStream.ToArray());
					result.IsSuccess = true;
				}
			}
			catch (Exception ex)
			{
				result.ErrorMessage = "Звони Максу, шото поломалось :)";
				logger.LogError(ex, $"Failed to create documents pack");
			}

			return result;
		}

		private string GetCustomerData(CustomerViewModel customer, bool forHeader = false)
		{
			var customerData = new StringBuilder();
			switch (customer.Type)
			{
				case CustomerType.Organisation:
				case CustomerType.SelfEmployed:
				{
					customerData.AppendLine($"{customer.Name}, ");
					customerData.AppendLine($"{customer.Address}, ");
					customerData.AppendLine($"УНП {customer.UNP}, ");
					customerData.AppendLine($"тел. {customer.Phone}, {(string.IsNullOrWhiteSpace(customer.Email) ? string.Empty : $"e-mail: {customer.Email},")}");
					customerData.AppendLine($"р/с {customer.CheckingAccount} в {customer.BankName}, {customer.BankAddress}, ");
					customerData.AppendLine($"БИК {customer.BankCode}");
					break;
				}
				case CustomerType.Person:
				{
					customerData.Append($"гр. {customer.Name}, ");
					if (customer.BirthDate.HasValue)
					{
						customerData.Append($"{customer.BirthDate.Value} года рождения, ");
					}
					customerData.Append($"паспорт {customer.PassportNumber}, ");
					customerData.Append($"выдан {customer.Delegate}, ");
					customerData.Append($"идентификационный № {customer.PassportUniqueCode}, ");
					customerData.Append($"зарегистрирован по адресу: {customer.RegistrationAddress}");
					if (!forHeader)
					{
						customerData.Append($", тел. {customer.Phone}, {(string.IsNullOrWhiteSpace(customer.Email) ? string.Empty : $"e-mail: {customer.Email}")}");
					}

					break;
				}
			}

			return customerData.ToString();
		}

		private IEnumerable<OrderItemForInvoiceViewModel> GetInvoicePositions(OrderViewModel order)
		{
			var result = new List<OrderItemForInvoiceViewModel>();
			var itemsLeftToAdd = order.OrderItems;

			if (order.IsLicenseProductsGrouped && order.IsCertificateNeeded)
			{
				itemsLeftToAdd = order.OrderItems.Where(x => !x.Product.IsLicenseRequired).ToList();
				var productExample = order.OrderItems.First(x => x.Product.IsLicenseRequired).Product;
				var totalQuantity = order.OrderItems.Where(x => x.Product.IsLicenseRequired).Sum(x => x.Quantity);
				var commonProduct = new OrderItemForInvoiceViewModel
				{
					Name = ProductsWithSertificateCommonName,
					Article = ProductWithSertificateCommonArticle,
					Price = productExample.Price - productExample.Price * order.Discount,
					Quantity = totalQuantity,
					Unit = productExample.Unit,
					TotalPlaces = Math.Round(totalQuantity / productExample.UnitsPerPlace, 0, MidpointRounding.ToPositiveInfinity),
					TotalWeight = Math.Round(productExample.WeightPerUnit * totalQuantity, 0)
				};

				result.Add(commonProduct);
			}

			result.AddRange(itemsLeftToAdd.Select(x => new OrderItemForInvoiceViewModel
			{
				Name = x.Product.Name,
				Article = x.Product.Article,
				Price = x.Product.Price - x.Product.Price * order.Discount,
				Quantity = x.Quantity,
				Unit = x.Product.Unit,
				TotalPlaces = Math.Round(x.Quantity / x.Product.UnitsPerPlace, 0, MidpointRounding.ToPositiveInfinity),
				TotalWeight = Math.Round(x.Product.WeightPerUnit * x.Quantity, 0)
			}));

			return result;
		}

		private IEnumerable<OrderItemForInvoiceViewModel> GetInvoiceSinglePositions(OrderViewModel order)
		{
			var result = new List<OrderItemForInvoiceViewModel>();
			var itemsLeftToAdd = order.OrderItems;

			if (order.IsLicenseProductsGrouped && order.IsCertificateNeeded)
			{
				itemsLeftToAdd = order.OrderItems.Where(x => !x.Product.IsLicenseRequired).ToList();
				var productExample = order.OrderItems.First(x => x.Product.IsLicenseRequired).Product;
				var totalQuantity = 1;
				var commonProduct = new OrderItemForInvoiceViewModel
				{
					Name = ProductsWithSertificateCommonName,
					Article = ProductWithSertificateCommonArticle,
					Price = productExample.Price - productExample.Price * order.Discount,
					Quantity = totalQuantity,
					Unit = productExample.Unit,
					TotalPlaces = Math.Round(totalQuantity / productExample.UnitsPerPlace, 0, MidpointRounding.ToPositiveInfinity),
					TotalWeight = Math.Round(productExample.WeightPerUnit * totalQuantity, 0)
				};

				result.Add(commonProduct);
			}

			result.AddRange(itemsLeftToAdd.Select(x => new OrderItemForInvoiceViewModel
			{
				Name = x.Product.Name,
				Article = x.Product.Article,
				Price = x.Product.Price - x.Product.Price * order.Discount,
				Quantity = 1,
				Unit = x.Product.Unit,
				TotalPlaces = Math.Round(1 / x.Product.UnitsPerPlace, 0, MidpointRounding.ToPositiveInfinity),
				TotalWeight = Math.Round(x.Product.WeightPerUnit * 1, 0)
			}));

			return result;
		}

		private IEnumerable<string> GetCertificateTableHeaders(IEnumerable<OrderItemViewModel> orderItems, IEnumerable<string> baseValues)
		{
			var positionsInOrder = orderItems.Select(x => 
				Regex.Split(x.Product.Name, @"[^0-9\,]+")
					.Where(c => c != "." && c.Trim() != ""))
					.Select(x => x.First().ToString());

			var intersectValues = positionsInOrder.Intersect(baseValues);
			if (intersectValues.Count() >= CertificateDefaultTableLenght)
			{
				return intersectValues;
			}

			var list = intersectValues.ToList();
			var headersCount = baseValues.Count() < CertificateDefaultTableLenght
				? baseValues.Count()
				: CertificateDefaultTableLenght;
			while (list.Count < headersCount)
			{
				list.Add(baseValues.First(x => !list.Contains(x)));
			}

			list.Sort();
			list.Reverse();

			return list;
		}

		public string FirstToUpper(string input)
		{
			if (string.IsNullOrWhiteSpace(input))
			{
				throw new ArgumentException($"{nameof(input)} should be provided", nameof(input));
			}

			return input.First().ToString().ToUpper() + string.Join("", input.Skip(1));
		}
	}

	public static class DocumentConsts
	{
		// Contract data
		// ----------------------------------------------------------------------------
		public const string SelfEmployedActReason = "действующий на основании свидетельства  о государственной регистрации  индивидуального предпринимателя с регистрационным номером №";
		public const string OrganisationActReason = "действующего на основании Устава";
		public const string OrganisationBuyReason = "«Покупатель» приобретает товар для оптовой и розничной торговли";
		public const string PersonBuyReason = "«Покупатель» приобретает товар для собственного потребления";
		public const string Prepayment = "«Покупатель» производит оплату за поставляемый по настоящему договору товар на условиях 100% предварительной оплаты, путем перечисления денежных средств на расчетный счет «Изготовителя» согласно выставленных счетов-фактур";
		public const string DefaultPayment = "«Покупатель» производит оплату за поставленный товар в течение пяти банковских дней от даты отгрузки товара";

		public const string ContractNumberTemplate = "{contractNumber}";
		public const string DateTemplate = "{date}";
		public const string ContractEndDateTemplare = "{contractEndDate}";
		public const string CustomerNameTemplate = "{customerName}";
		public const string DelegateTemplate = "{delegate}";
		public const string ActReasonTemplate = "{actReason}";
		public const string BuyReasonTemplate = "{buyReason}";
		public const string PaymentTypeTemplate = "{paymentType}";
		public const string CustomerDataTemplate = "{customerData}";
		// ---------------------------------------------------------------------------

		// Agreement data
		// ---------------------------------------------------------------------------
		public const string AgreementNumberTemplate = "{agreementNumber}";
		public const string ContractDateTemplare = "{contractDate}";
		// ---------------------------------------------------------------------------

		// Certificate data
		// ---------------------------------------------------------------------------
		public const string CertificateNumberTemplate = "{certificateNumber}";
		public const string TotalWeightTemplate = "{totalWeight}";
		public const string DrobTableTemplate = "{drobTable}";
		public const string KartechTableTemplate = "{kartechTable}";
		// ---------------------------------------------------------------------------

		// Invoice data
		// ---------------------------------------------------------------------------
		// First page
		public const string CustomerUnpCell = "BM5";
		public const string InvoiceDayCell = "D15";
		public const string InvoiceMonthCell = "K15";
		public const string InvoiceYearCell = "AK15";
		public const string CarCell = "N17";
		public const string WaybillCell = "DO17";
		public const string DriverFirstCell = "K20";
		public const string CustomerNameAndAddressCell = "S29";
		public const string ContractNumberAndDateCell = "T32";
		public const string DeliveryAddressCell = "DO32";
		public const string TotalNdsAmountRubCell = "S46";
		public const string TotalNdsAmountCentsCell = "EM46";
		public const string TotalAmountNdsIncludingRubCell = "X48";
		public const string TotalAmountNdsIncludingCentsCell = "EM48";

		public const string TotalAmountTableCell = "AX43";
		public const string TotalPriceTableCell = "BT43";
		public const string TotalNdsAmountTableCell = "CP43";
		public const string TotalAmountWithNdsTableCell = "DC43";
		public const string TotalPlacesTableCell = "DQ43";
		public const string TotalWeightTableCell = "EA43";

		// Second page
		public const string TotalWeightCell = "V55";
		public const string TotalPlacesCell = "DA55";
		public const string DriverSecondCell = "CZ57";
		public const string DocsAttachedCell = "AI106";
		// ---------------------------------------------------------------------------
	}
}
