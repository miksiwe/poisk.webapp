﻿using AutoMapper;
using Microsoft.Extensions.Logging;
using Microsoft.VisualBasic;
using poisk.webapp.Database;
using poisk.webapp.Middlewares;
using poisk.webapp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using poisk.webapp.Database.Entities;

namespace poisk.webapp.Services
{
	public interface IOrderService
	{
		ServiceResponseModel<IEnumerable<OrderViewModel>> Get();

		ServiceResponseModel<OrderViewModel> Add(OrderViewModel order);

		ServiceResponseModel Update(OrderViewModel order);

		ServiceResponseModel Delete(int id);

		ServiceResponseModel<int> GetDocumentNextNumber(int customerId);
	}

	public class OrderService : IOrderService
	{
		private readonly MainDbContext context;
		private readonly ILogger logger;
		private readonly IMapper mapper;

		public OrderService(MainDbContext context, IMapper mapper, ILogger<OrderService> logger)
		{
			this.context = context ?? throw new ArgumentNullException(nameof(context));
			this.logger = logger ?? throw new ArgumentNullException(nameof(logger));
			this.mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
		}

		public ServiceResponseModel<IEnumerable<OrderViewModel>> Get()
		{
			var response = new ServiceResponseModel<IEnumerable<OrderViewModel>>();

			try
			{
				var entities = context.Orders
					.Include(x => x.OrderItems)
						.ThenInclude(x => x.Product)
					.Include(x => x.Customer)
					.OrderByDescending(x => x.Id)
					.AsNoTracking()
					.ToList();
				var orders = entities.Select(x => mapper.Map<OrderViewModel>(x));

				response.IsSuccess = true;
				response.Data = orders;
				return response;
			}

			catch (Exception e)
			{
				logger.LogError(e, "Unable to get orders");
				response.ErrorMessage = "Не удалось загрузить заказы";
				return response;
			}
		}

		public ServiceResponseModel<OrderViewModel> Add(OrderViewModel order)
		{
			var response = new ServiceResponseModel<OrderViewModel>();
			if (order == null)
			{
				return response;
			}

			order.CreatedAt = DateTime.Now;
			var entity = mapper.Map<Order>(order);
			try
			{
				context.Add(entity);
				context.SaveChanges();

				order.Id = entity.Id;
				response.IsSuccess = true;
				response.Data = order;
				return response;
			}
			catch (Exception e)
			{
				logger.LogError(e, "Unable to save order");
				response.ErrorMessage = "Не удалось добавить заказ";
				return response;
			}
		}

		public ServiceResponseModel Update(OrderViewModel order)
		{
			var response = new ServiceResponseModel();
			if (order == null)
			{
				return response;
			}

			if (!order.FinishedAt.HasValue && order.State == SharedData.DoneOrderStatus)
			{
				order.FinishedAt = DateTime.Now;
			}

			var entity = mapper.Map<Order>(order);
			try
			{
				using (var transaction = context.Database.BeginTransaction())
				{
					var orderItems = context.OrderItems.Where(x => x.OrderId == order.Id);
					context.OrderItems.RemoveRange(orderItems);
					context.SaveChanges();

					context.DetachLocal(entity);
					context.DetachLocal(entity.Customer);

					context.Orders.Update(entity);
					context.SaveChanges();

					transaction.Commit();
				}

				response.IsSuccess = true;
				return response;
			}
			catch (Exception e)
			{
				logger.LogError(e, "Unable to save customer");
				response.ErrorMessage = "Не удалось изменить клиента";
				return response;
			}
		}

		public ServiceResponseModel Delete(int id)
		{
			var response = new ServiceResponseModel();
			if (id == default)
			{
				return response;
			}

			try
			{
				var entity = context.Orders.Single(x => x.Id == id);
				context.DetachLocal(entity);
				context.Remove(entity);
				context.SaveChanges();

				response.IsSuccess = true;
				return response;
			}
			catch (Exception e)
			{
				logger.LogError(e, "Unable to remove order");
				response.ErrorMessage = "Не удалось удалить заказ";
				return response;
			}
		}

		public ServiceResponseModel<int> GetDocumentNextNumber(int customerId)
		{
			var response = new ServiceResponseModel<int>();
			if (customerId == default)
			{
				return response;
			}

			try
			{
				var order = context.Orders
					.AsNoTracking()
					.Where(x => x.CustomerId == customerId && x.CreatedAt.Year == DateTime.Now.Year)
					.OrderByDescending(x => x.CreatedAt)
					.LastOrDefault();

				if (order != null && int.TryParse(order.AgreementNumber, out var number))
				{
					response.Data = number + 1;
				}
				else
				{
					response.Data = 1;
				}

				response.IsSuccess = true;
				return response;
			}
			catch (Exception e)
			{
				logger.LogError(e, "Unable to obtain orders count");
				response.ErrorMessage = "Не удалось получить номера документов";
				return response;
			}
		}
	}
}
