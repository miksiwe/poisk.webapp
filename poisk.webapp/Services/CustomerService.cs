﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using poisk.webapp.Database;
using poisk.webapp.Database.Entities;
using poisk.webapp.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace poisk.webapp.Services
{
    public interface ICustomerService
    {
        ServiceResponseModel<IEnumerable<CustomerViewModel>> Get();

        ServiceResponseModel<CustomerViewModel> Add(CustomerViewModel customer);

        ServiceResponseModel Update(CustomerViewModel customer);

        ServiceResponseModel Delete(int id);
    }

    public class CustomerService : ICustomerService
    {
        private readonly MainDbContext context;
        private readonly ILogger logger;
        private readonly IMapper mapper;

        public CustomerService(MainDbContext context, IMapper mapper, ILogger<ProductService> logger)
        {
            this.context = context ?? throw new ArgumentNullException(nameof(context));
            this.logger = logger ?? throw new ArgumentNullException(nameof(logger));
            this.mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public ServiceResponseModel<CustomerViewModel> Add(CustomerViewModel customer)
        {
            var response = new ServiceResponseModel<CustomerViewModel>();
            if (customer == null)
            {
                return response;
            }

            var entity = mapper.Map<Customer>(customer);

            try
            {
                context.Add(entity);
                context.SaveChanges();

                customer.Id = entity.Id;
                response.IsSuccess = true;
                response.Data = customer;
                return response;
            }
            catch (Exception e)
            {
                logger.LogError(e, "Unable to save customer");
                response.ErrorMessage = "Не удалось добавить клиента";
                return response;
            }
        }

        public ServiceResponseModel Delete(int id)
        {
            var response = new ServiceResponseModel();
            if (id == default)
            {
                return response;
            }

            try
            {
                var entity = context.Customers.Single(x => x.Id == id);
                context.DetachLocal(entity);
                context.Remove(entity);
                context.SaveChanges();

                response.IsSuccess = true;
                return response;
            }
            catch (Exception e)
            {
                logger.LogError(e, "Unable to remove customer");
                response.ErrorMessage = "Не удалось удалить клиента";
                return response;
            }
        }

        public ServiceResponseModel<IEnumerable<CustomerViewModel>> Get()
        {
            var response = new ServiceResponseModel<IEnumerable<CustomerViewModel>>();

            try
            {
                var entities = context.Customers.AsNoTracking().ToList();
                var customers = entities.Select(x => mapper.Map<CustomerViewModel>(x));

                response.IsSuccess = true;
                response.Data = customers;
                return response;
            }
            catch (Exception e)
            {
                logger.LogError(e, "Unable to get customers");
                response.ErrorMessage = "Не удалось загрузить клиентов";
                return response;
            }
        }

        public ServiceResponseModel Update(CustomerViewModel customer)
        {
            var response = new ServiceResponseModel();
            if (customer == null)
            {
                return response;
            }

            var entity = mapper.Map<Customer>(customer);

            try
            {
                context.DetachLocal(entity);
                context.Customers.Update(entity);
                context.SaveChanges();

                response.IsSuccess = true;
                return response;
            }
            catch (Exception e)
            {
                logger.LogError(e, "Unable to save customer");
                response.ErrorMessage = "Не удалось добавить клиента";
                return response;
            }
        }
    }
}
