﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using poisk.webapp.Middlewares;
using poisk.webapp.Models;

namespace poisk.webapp.Pages
{
    public class LoginModel : PageModel
    {
        private readonly Tuple<string, string> credentials = new Tuple<string, string>("Olga", "compaq1");

        [BindProperty]
        public LoginViewModel LoginParams { get; set; }

        public async Task<IActionResult> OnGetAsync()
        {
            return await Task.FromResult(Page());
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            if (LoginParams.Login != credentials.Item1 || LoginParams.Password != credentials.Item2)
            {
                ModelState.AddModelError("", "Неверный логин или пароль");
                return Page();
            }

            var claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, LoginParams.Login),
            };

            var id = new ClaimsIdentity(claims, "ApplicationCookie");
            await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(id));

            return LocalRedirect(Url.Content("~/"));
        }
    }
}